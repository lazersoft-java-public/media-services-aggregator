@echo off

if not "%JAVA_HOME%" == "" goto OkJHome

:OkJHome
if exist "%JAVA_HOME%\bin\java.exe" goto performbuild

echo.
echo Error: JAVA_HOME is set to an invalid directory. >&2
echo JAVA_HOME = "%JAVA_HOME%" >&2
echo Please set the JAVA_HOME variable in your environment to match the >&2
echo location of your Java installation. >&2
echo.
goto error

:performbuild
echo ==== 
echo ==== Building client...
cd frontend
echo ==
echo == Checking necessary packages are present...
call npm install
echo ==
echo == Compiling...
call ng build --prod=true
cd ..\
echo ====
echo ==== Copy frontend static resources to backend...
echo ====
robocopy /S /E frontend\dist\frontend backend\src\main\resources\static
echo ==== Building back-end...
cd backend
call mvn clean package -DskipTests
title build - Media Service Aggregator
cd ..\
echo ====
echo ==== Cleaning up...
rmdir /s/q backend\src\main\resources\static
echo ====
echo ==== Build finished. Java executable available at: 
set root_path=%cd%
echo %root_path%\backend\target\
dir /b backend\target\*.jar

:error
set ERROR_CODE=1

:end
@endlocal & set ERROR_CODE=%ERROR_CODE%

exit /B %ERROR_CODE%
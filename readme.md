# Media Service Aggregator

## Prerequisites

This project requires **JDK version 1.8 64 bit**, and **NodeJS version 8.x or 10.x**

## Building the project

Execute **build.cmd** (if on Windows) or **build.sh** (if on Linux) from the project's root folder.

This command may take a long time to be executed (only for the first execution) as it might need to bring in npm packages and Maven dependencies.

## Launching the project

Open terminal in: 
```
[project root folder]/backend/target
```

Execute command:

```
java -jar media-services-aggregator-0.0.1-SNAPSHOT.jar
```

If the application doesn't start, complaining that the default (OS-level) encoding is not UTF-8, specify a default encoding of UTF-8 when starting it. Normally, on Linux systems the default is UTF-8, while on Windows systems it's not:
```
java -Dfile.encoding="UTF-8" -jar media-services-aggregator-0.0.1-SNAPSHOT.jar
```

The application will be available at

http://localhost:9090

## Dev environment for the project

The project consists of 2 modules: a "backend" and a "frontend".

#### Backend module

The "backend" is a Java project managed using Maven. The "frontend" is an Angular 6 (actual version: 7) project.

Make sure **JDK version 1.8 64 bit** and **Maven version 3.x** are installed.

To start the backend in dev mode, open a terminal in

```
[project root folder]/backend
```

and execute command:

```
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dfile.encoding=UTF-8" -Dspring-boot.run.profiles=dev
```

This will start the backend at address:

http://localhost:8080

A quick check that everything works ok can be performed by accessing in a browser the following link:

http://localhost:8080/echo/hi

This should return:

```
Echoing (dev): hi
```

#### Frontend module

Make sure **NodeJS version 8.x or 10.x** is installed.

To start the frontend in dev mode, open a terminal in

```
[project root folder]/frontend
```

and execute command:

```
ng serve --open
```

This will compile the module and open a browser at address:

http://localhost:4200

If everything worked OK, this should display the application's search page. Try a search!

##### Frontend module specs (Unit Tests)

Unit tests are implemented and ran using Jasmine, with Karma for browser interaction.

To run the specs with default configuration, simply execute:

```
ng test
```
This will run the tests in a (real) Chrom window, and then leave the windows open and watch for modifications in the code, which will automatically run the tests again.

For a CI environment, where we asume HEADLESS (no Chrome window can be started), 
and where the tests need to be run only once, and then the process finishes, while at the same time NOT watching for changes in file, you need to use the karma-headless.conf.js file, by running:

```
ng test --karmaConfig=src/karma-headless.conf.js
```

##### Frontend module end to end (e2e) tests

Unit tests are implemented and ran using Jasmine, with Protractor for browser interaction.

To run the specs with default configuration, simply execute:

```
ng e2e
```
This will run the tests in a (real) Chrom window.

For a CI environment, where we asume HEADLESS (no Chrome window can be started), you need to use the protractor-headless.conf.js file, by running:

```
ng e2e --protractorConfig=e2e/protractor-headless.conf.js
```

### Using IDEs for dev environment

The backend module can be imported in Eclipse (as an existing Maven project). An Eclipse launcher named "media-services-aggregator backend dev" is included in the project, for convenience.

The frontend module can be imported in VSCode (import project from folder). It has a convenience terminal task named "media-services-aggregator frontend dev" for launching the project, as well as the debug set to opening Chrome with TypeScript debug enabled (F5).

## Architecture and tech stack

#### Backend

The backend is created using Spring Boot. I believe Spring Boot greatly facilitates the initial creation of such a project.

The backend API is exposed as REST (using JSON) Endpoints. This is for decoupling of the frontend implementation.

The architecutre I've chosen was focused on consuming upstream media services (such as ITunes, Google Boooks. etc). The idea was to implement a simple and generic way to interact with such upstream services, as well as leave open the possibility to add other such services in the future (like maybe Spotify, etc).

As such, the data access layer is abstracted to a MediaClient service that can deal with HTTP/JSON requests with URL encoded parameters towards such upstream services. This layer is further abstracted to a MediaClientAdapter, than simply needs to have a function (lambda) for constructing a specific request from the generic request accepted by the backend, as well as another function (lambda) for converting the specific upstream response back to the generic response returned by the backend.

There are interceptors for various types of errors/exceptions, especially for those having to do with upstream services behavior (service timedout, or deemed the request as invalid etc). Their role is to transform such specific errors into generic application errors (of that specific type: timeout, invalid request, etc).

#### Frontend

I've chosen Angular version 7 for building the front-end. This is simply because I believe that Angular offers very good tools for quickly building a basic UI. It has a simple set of components for building the various parts of the UI (search input, search result list etc), as well as interceptors for dealing with errors, so that error handling per each type of media query (books, music, etc) is dealt with separately from the UI components business logic

## Future developments

Some of the other features that I believe would be usefull to add to such an application are:

- adding a cache layer, per upstream service.

This can easily be setup and configured with Spring Boot. Also, the current business use case is pertinent for such a cache: not returing the latest books or music from iTunes or Google Books is not something that would bother the majority of users. A few minutes or maybe hours delay, by configuring a cache with such eviction times, would be tolerable from this point of view, and will most likely improve performance, by cutting down on the time necessary to make the roundtrip call to each upstream service for each request.

- adding a detailed health check page

This can detail the current state of each upstream service (available/down, etc), as well as the state of cache(s) (how much they are filled, what cache keys are receiving the most hits, etc.). This could be further detailed with a back-end implementation that keeps track of request history for each upstream service (how many total requests were made towards them, how many returned error, what's the most frequent type of error, etc). Again, I believe Spring Boot and Spring itself can greatly facilitate such an implemnetaiton (via a simple storage mechanism, and interceptors) 

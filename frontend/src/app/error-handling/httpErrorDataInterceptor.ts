import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpErrorResponse,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {  InterceptedHttpErrorDataEmitter } from './interceptedHttpErrorDataEmitter';
import { ErrorType } from './errorType';
import { HttpErrorData } from './httpErrorData';

@Injectable()
export class HttpErrorDataInterceptor implements HttpInterceptor {

    constructor(private interceptedHttpErrorDataEmitter: InterceptedHttpErrorDataEmitter) {

    }

    intercept(
        request: HttpRequest<any>,
        next: HttpHandler
      ): Observable<HttpEvent<any>> {

        return next.handle(request)
            .pipe(catchError((error, caught) => {
                if (error instanceof HttpErrorResponse) {
                    try {
                        const requestUrl = request.urlWithParams;
                        const requestBody = request.body;
                        const status: number = error.status;
                        const statusText = error.statusText;
                        let errorType: ErrorType = null;
                        let errorTypeDescription: string = null;
                        let errorMessage: ErrorType = null;
                        if (error.error != null) {
                            errorType = error.error.errorType;
                            errorTypeDescription = error.error.errorTypeDescription;
                            errorMessage = error.error.errorMessage;
                        }
                        const httpErrorData = new HttpErrorData(errorType, errorTypeDescription, errorMessage,
                            status, statusText, requestUrl, requestBody);
                        this.interceptedHttpErrorDataEmitter.onErrorDataIntercepted(httpErrorData);
                    } catch (e) {
                        console.error(e);
                    }
                }
                return of(error);
            }));

      }

}

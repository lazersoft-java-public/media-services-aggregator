export enum ErrorType {

    MEDIA_SEARCH_INVALID_REQUEST_EXCEPTION = 'MEDIA_SEARCH_INVALID_REQUEST_EXCEPTION',

    MEDIA_PROVIDER_CLIENT_TIMEOUT = 'MEDIA_PROVIDER_CLIENT_TIMEOUT',
    MEDIA_PROVIDER_CLIENT_INVALID_REQUEST = 'MEDIA_PROVIDER_CLIENT_INVALID_REQUEST',
    MEDIA_PROVIDER_CLIENT_ERROR = 'MEDIA_PROVIDER_CLIENT_ERROR',

    APPLICATION_ERROR = 'APPLICATION_ERROR'
}

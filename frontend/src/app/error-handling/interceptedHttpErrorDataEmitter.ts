import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpErrorData } from './httpErrorData';

@Injectable({
    providedIn: 'root'
})
export class InterceptedHttpErrorDataEmitter {
  @Output() emitter = new EventEmitter<HttpErrorData>();

  public onErrorDataIntercepted(errorData: HttpErrorData) {
    this.emitter.next(errorData);
  }
}

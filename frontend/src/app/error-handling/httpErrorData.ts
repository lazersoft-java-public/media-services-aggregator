import { ErrorType } from './errorType';

export class HttpErrorData {
    errorType: ErrorType;
    errorTypeDescription: string;
    errorMessage: string;
    status: number;
    statusText: string;
    requestUrl: string;
    requestBody: any;

    constructor(errorType: ErrorType, errorTypeDescription: string, errorMessage: string,
      status: number, statusText: string, requestUrl: string, requestBody: any) {
      this.errorType = errorType;
      this.errorTypeDescription = errorTypeDescription;
      this.errorMessage = errorMessage;
      this.status = status;
      this.statusText = statusText;
      this.requestUrl = requestUrl;
      this.requestBody = requestBody;
    }
  }

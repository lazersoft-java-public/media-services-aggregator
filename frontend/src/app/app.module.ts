import { BrowserModule, HAMMER_LOADER } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS  } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule} from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule,
  MatRippleModule,
  MatIconModule,
  MatCardModule,
  MatGridListModule,
  MatListModule,
  MatTooltipModule,
  MatProgressSpinnerModule } from '@angular/material';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EchoComponent } from './echo/echo.component';
import { SearchComponent } from './search/search.component';
import { SearchResultsComponent } from './search-results/search-results.component';

import { HttpErrorDataInterceptor} from './error-handling/httpErrorDataInterceptor';

@NgModule({
  declarations: [
    AppComponent,
    EchoComponent,
    SearchComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule,
    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatInputModule,
    MatRippleModule,
    MatListModule,
    MatGridListModule,
    MatTooltipModule,
    MatProgressSpinnerModule
  ],
  providers: [{
    provide: HAMMER_LOADER,
    useValue: () => new Promise(() => {})
  },
  { provide: HTTP_INTERCEPTORS, useClass: HttpErrorDataInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

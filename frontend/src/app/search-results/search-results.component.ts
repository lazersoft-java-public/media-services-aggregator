import { Component, OnInit, Input } from '@angular/core';
import { MediaEntry } from '../search/mediaEntry';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  @Input() mediaEntries: Array<MediaEntry>;

  constructor() { }

  ngOnInit() {
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EchoService {

  constructor(private http: HttpClient) { }

  public doEcho(message: string): Observable<string> {
    return this.http.get(environment.backend.rootUrl + environment.backend.endpoint.echo.urlSuffix + '/' + message,
      {
        responseType: 'text'
      });
  }
}

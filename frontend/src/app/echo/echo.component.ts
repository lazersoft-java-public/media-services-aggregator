import { Component, OnInit, Input } from '@angular/core';
import { EchoService } from './service/echo.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-echo',
  templateUrl: './echo.component.html',
  styleUrls: ['./echo.component.css']
})
export class EchoComponent implements OnInit {

  @Input() message = 'Hello';
  response = 'no response yet';

  env: Object;

  constructor(private echoService: EchoService) { }

  ngOnInit() {
    this.doEcho(this.message);
    this.env = environment;
  }

  onEchoRequested(): void {
    this.doEcho(this.message);
  }

  private doEcho(message: string): void {
    this.echoService.doEcho(message).subscribe(echoResponse => {
      this.response = echoResponse;
    });
  }

}

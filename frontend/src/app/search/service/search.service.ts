import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { SearchRequest } from './searchRequest';
import { SearchResponse } from './searchResponse';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private searchUrl: string;

  constructor(private http: HttpClient) {
    const env = environment;
    this.searchUrl = env.backend.rootUrl + env.backend.endpoint.media.urlSuffix + env.backend.endpoint.media.search.urlSuffix;
   }

  public search(request: SearchRequest): Observable<Object> {
    return this.http.post<SearchResponse>(this.searchUrl, request);
  }
}

export class SearchResponseEntry {
  title: string;
  authorsOrArtists: Array<string>;
}

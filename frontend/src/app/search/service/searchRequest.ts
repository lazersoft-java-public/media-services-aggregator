import { MediaType } from '../MediaType';

export class SearchRequest {
  searchTerm: string;
  mediaType: MediaType;
  maxResults: number;

  constructor(searchTerm: string, mediaType: MediaType, maxResults: number) {
    this.searchTerm = searchTerm;
    this.mediaType = mediaType;
    this.maxResults = maxResults;
  }
}

import { SearchResponseEntry } from './searchResponseEntry';

export class SearchResponse {
  entries: Array<SearchResponseEntry>;
}

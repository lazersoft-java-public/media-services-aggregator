import { MediaType } from './MediaType';
import { SearchResponseEntry } from './service/searchResponseEntry';

export class MediaEntry {
  title: string;
  authorsOrArtists: Array<string>;
  mediaType: string;

  constructor(searchResponseEntry: SearchResponseEntry, mediaType: MediaType) {
    this.title = searchResponseEntry.title;
    this.authorsOrArtists = searchResponseEntry.authorsOrArtists;
    this.mediaType = mediaType;
  }
}

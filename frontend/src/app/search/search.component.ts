import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from './service/search.service';
import { SearchRequest } from './service/searchRequest';
import { environment } from 'src/environments/environment';
import { SearchResponse } from './service/searchResponse';
import { MediaEntry } from './mediaEntry';
import { InterceptedHttpErrorDataEmitter } from '../error-handling/interceptedHttpErrorDataEmitter';
import { HttpErrorData } from '../error-handling/httpErrorData';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  @Input() searchTerm: string;

  private env: any;
  mediaEntries: Array<MediaEntry>;
  runningSearches: number;
  searchErrors: Array<String>;
  firstSuccessfulSearchPerformed: boolean;

  constructor(private searchService: SearchService, private interceptedHttpErrorDataEmitter: InterceptedHttpErrorDataEmitter) { }

  ngOnInit() {
    this.env = environment;
    this.mediaEntries = new Array<MediaEntry>();
    this.runningSearches = 0;
    this.searchErrors = new Array<string>();
    this.interceptedHttpErrorDataEmitter.emitter.subscribe((httpErrorData) => { this.onHttpErrorData(httpErrorData); });
  }

  public search(): void {
    this.clearArray(this.mediaEntries);
    this.clearArray(this.searchErrors);
    this.runningSearches = 0;
    this.env.mediaTypes.forEach( mediaType => {
      this.runningSearches++;
      this.searchService.search(new SearchRequest(this.searchTerm,
        mediaType,
        this.env.searchMaxResultCountPerMediaType)).subscribe((receivedSearchResponse: SearchResponse) => {
          receivedSearchResponse.entries.forEach(entry => {
            this.mediaEntries.push(new MediaEntry(entry, mediaType));
          });
          this.sortMediaEntries();
          this.decrementRunningSearches();
          this.firstSuccessfulSearchPerformed = true;
        });
    });
  }

  public clearSearchInput() {
    this.searchTerm = '';
  }

  public isSearchTermValid(): boolean {
    return this.searchTerm != null && this.searchTerm.trim().length > 0;
  }

  private clearArray(array: Array<any>): void {
    while (array.length > 0) {
      array.pop();
    }
  }

  private sortMediaEntries(): void {
    this.mediaEntries.sort((a, b) => (a.title > b.title ? 1 : -1));
  }

  private onHttpErrorData(data: HttpErrorData) {
    if (data.requestBody instanceof SearchRequest) {
      this.decrementRunningSearches();
      this.searchErrors.push('We\'ve had an error getting your ' +
        this.env.mediaTypeDescriptions[data.requestBody.mediaType] + ': ' + data.errorTypeDescription);
    }
  }

  private decrementRunningSearches() {
    if (this.runningSearches > 0) {
      this.runningSearches--;
    }
  }

}

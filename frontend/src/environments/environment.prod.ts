import { MediaType } from 'src/app/search/MediaType';

export const environment = {
  production: true,
  backend: {
    rootUrl: '',
    endpoint: {
      echo: {
        urlSuffix: '/echo'
      },
      media: {
        urlSuffix: '/media',
        search: {
          urlSuffix: '/search'
        }
      }
    }
  },
  searchMaxResultCountPerMediaType: 5,
  mediaTypes: [MediaType.BOOKS, MediaType.MUSIC_ALBUMS],
  mediaTypeDescriptions: new Map([[MediaType.BOOKS, 'books'], [MediaType.MUSIC_ALBUMS, 'music albums']])
};

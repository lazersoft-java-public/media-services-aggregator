#!/bin/bash
if [[ -z "${JAVA_HOME}"} ]]; then
	echo "Error: JAVA_HOME not set" 1>&2
	exit 1
fi
	
echo "====" 
echo "==== Building client..."
cd frontend
echo "=="
echo "== Checking necessary packages are present..."
npm install
echo "=="
echo "== Compiling..."
ng build --prod=true
cd ../
echo "===="
echo "==== Copy frontend static resources to backend..."
echo "===="
cp -r frontend/dist/frontend backend/src/main/resources/static
echo "==== Building back-end..."
cd backend
mvn clean package -DskipTests
cd ../
echo "===="
echo "==== Cleaning up..."
rm -rf backend/src/main/resources/static
echo "===="
echo "==== Build finished. Java executable available at:" 
ROOT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo "${ROOT_DIR}/backend/target/"
JAR_NAME="$(ls ${ROOT_DIR}/backend/target/*.jar)"
echo ${JAR_NAME}
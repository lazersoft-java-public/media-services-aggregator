package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient;

import java.net.SocketTimeoutException;
import java.net.URI;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientInvalidRequestException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientTimeoutException;
import andreib.mediaservicesaggregator.service.media.codec.URIQueryValueCodec;

public class MediaProviderClientTest {

	private MediaProviderClient<SearchFieldMock, MediaProviderClientResponseMock> mediaProviderClient;
	private RestTemplate restTemplate;

	@Test
	public void testSearch_regular_text_in_search() {
		restTemplate = mockRestTemplate(new MediaProviderClientResponseMock());
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("value1");
		mediaProviderClient.search(searchRequest);

		Mockito.verify(restTemplate).exchange(URI.create("myBaseUrl?field1=value1"), HttpMethod.GET, null,
				MediaProviderClientResponseMock.class);
	}

	@Test
	public void testSearch_regular_text_utf8() {
		restTemplate = mockRestTemplate(new MediaProviderClientResponseMock());
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("我等你");
		mediaProviderClient.search(searchRequest);

		Mockito.verify(restTemplate).exchange(URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("我等你")),
				HttpMethod.GET, null, MediaProviderClientResponseMock.class);
	}

	@Test
	public void testSearch_encodable_character_utf8() {
		restTemplate = mockRestTemplate(new MediaProviderClientResponseMock());
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("René Descartes");
		mediaProviderClient.search(searchRequest);

		Mockito.verify(restTemplate).exchange(
				URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("René Descartes")), HttpMethod.GET,
				null, MediaProviderClientResponseMock.class);
	}

	@Test
	public void testSearch_encodable_characters_utf8() {
		restTemplate = mockRestTemplate(new MediaProviderClientResponseMock());
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("c++");
		mediaProviderClient.search(searchRequest);

		Mockito.verify(restTemplate).exchange(URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("c++")),
				HttpMethod.GET, null, MediaProviderClientResponseMock.class);
	}

	@Test(expected = MediaProviderClientTimeoutException.class)
	public void testHandleException_timeout() {
		restTemplate = mockRestTemplate(new RestClientException("", new SocketTimeoutException()));
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("doesn't matter");
		mediaProviderClient.search(searchRequest);

		restTemplate.exchange(URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("c++")), HttpMethod.GET,
				null, MediaProviderClientResponseMock.class);
	}

	@Test(expected = MediaProviderClientInvalidRequestException.class)
	public void testHandleException_invalid_request() {
		restTemplate = mockRestTemplate(
				HttpClientErrorException.create(HttpStatus.BAD_REQUEST, null, null, null, null));
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("doesn't matter");
		mediaProviderClient.search(searchRequest);

		restTemplate.exchange(URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("c++")), HttpMethod.GET,
				null, MediaProviderClientResponseMock.class);
	}

	@Test(expected = MediaProviderClientException.class)
	public void testHandleException_general_error() {
		restTemplate = mockRestTemplate(
				HttpClientErrorException.create(HttpStatus.UNSUPPORTED_MEDIA_TYPE, null, null, null, null));
		mediaProviderClient = newMediaProviderClient(restTemplate);

		MediaProviderSearchRequest<SearchFieldMock> searchRequest = mockMediaProviderSearchRequest("doesn't matter");

		mediaProviderClient.search(searchRequest);
		restTemplate.exchange(URI.create("myBaseUrl?field1=" + new URIQueryValueCodec().encode("c++")), HttpMethod.GET,
				null, MediaProviderClientResponseMock.class);
	}

	private RestTemplate mockRestTemplate(MediaProviderClientResponseMock responseBody) {
		RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
		Mockito.when(restTemplate.exchange(Mockito.any(URI.class), Mockito.any(HttpMethod.class),
				Mockito.nullable(HttpEntity.class), Mockito.eq(MediaProviderClientResponseMock.class)))
				.thenReturn(new ResponseEntity<>(responseBody, HttpStatus.OK));
		return restTemplate;
	}

	private RestTemplate mockRestTemplate(RestClientException executeException) {
		RestTemplate restTemplate = Mockito.mock(RestTemplate.class);
		Mockito.when(restTemplate.exchange(Mockito.any(URI.class), Mockito.any(HttpMethod.class),
				Mockito.nullable(HttpEntity.class), Mockito.eq(MediaProviderClientResponseMock.class)))
				.thenThrow(executeException);
		return restTemplate;
	}

	private MediaProviderClient<SearchFieldMock, MediaProviderClientResponseMock> newMediaProviderClient(
			RestTemplate restTemplate) {
		return new MediaProviderClientImpl(restTemplate, HttpMethod.GET, "myBaseUrl",
				MediaProviderClientResponseMock.class);
	}

	private MediaProviderSearchRequest<SearchFieldMock> mockMediaProviderSearchRequest(String searchTerm) {
		MediaProviderSearchRequest<SearchFieldMock> searchRequest = new MediaProviderSearchRequest.Builder<SearchFieldMock>()
				.setFieldValue(new SearchFieldMock("field1"), searchTerm).build();
		return searchRequest;
	}

	private static class SearchFieldMock implements TextConvertable {
		private final String fieldName;

		private SearchFieldMock(String fieldName) {
			this.fieldName = fieldName;
		}

		@Override
		public String getText() {
			return fieldName;
		}
	}

	@SuppressWarnings("unused")
	private static class MediaProviderClientResponseMock {
		private String field;

		public String getField() {
			return field;
		}

		public void setField(String field) {
			this.field = field;
		}

	}

	private static class MediaProviderClientImpl
			extends MediaProviderClient<SearchFieldMock, MediaProviderClientResponseMock> {

		protected MediaProviderClientImpl(RestTemplate restTemplate, HttpMethod httpMethod, String baseUrl,
				Class<MediaProviderClientResponseMock> responseClass) {
			super(restTemplate, httpMethod, baseUrl, responseClass);
		}

	}
}

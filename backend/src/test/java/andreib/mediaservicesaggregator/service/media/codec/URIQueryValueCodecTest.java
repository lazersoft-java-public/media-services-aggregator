package andreib.mediaservicesaggregator.service.media.codec;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import andreib.mediaservicesaggregator.Application;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@ActiveProfiles(Application.DEV_PROFILE)
public class URIQueryValueCodecTest {

	@Autowired
	private URIQueryValueCodec uriQueryValueCodec;

	@Test
	public void testEncode_single_special_character() {
		String value = "René Descartes";
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "René%20Descartes";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}

	@Test
	public void testEncode_two_consecutive_special_characters() {
		String value = "++i";
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "%2B%2Bi";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}

	@Test
	public void testEncode_null_string() {
		String value = null;
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}

	@Test
	public void testEncode_empty_string() {
		String value = "";
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}

	@Test
	public void testEncode_empty_string_space() {
		String value = " ";
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}

	@Test
	public void testEncode_empty_string_spaces() {
		String value = "  ";
		String encodedValue = uriQueryValueCodec.encode(value);
		String expectedEncodedValue = "";
		assertThat(encodedValue, is(equalTo(expectedEncodedValue)));
	}
}

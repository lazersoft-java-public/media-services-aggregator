package andreib.mediaservicesaggregator.service.media.providerclientadapter;

import org.junit.Test;
import org.mockito.Mockito;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderClient;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderSearchRequest;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;
import andreib.mediaservicesaggregator.service.media.MediaSearchRequest;

public class MediaProviderClientAdapterTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testMediaSearchRequestConvertion() {
		MediaProviderClient<TextConvertable, Object> mediaProviderClientMock = Mockito.mock(MediaProviderClient.class);
		Mockito.when(mediaProviderClientMock.search(Mockito.any())).thenReturn(new Object());
		MediaSearchRequestConverter<TextConvertable> mediaSearchRequestConverter = Mockito
				.mock(MediaSearchRequestConverter.class);
		Mockito.when(mediaSearchRequestConverter.convert(Mockito.any())).thenReturn(newMediaProviderSearchRequest());
		MediaProviderSearchResponseConverter<Object> mediaProviderSearchResponseConverterMock = Mockito
				.mock(MediaProviderSearchResponseConverter.class);

		MediaProviderClientAdapter<TextConvertable, Object> adapter = new MediaProviderClientAdapter<>(MediaType.BOOKS,
				mediaProviderClientMock, mediaSearchRequestConverter, mediaProviderSearchResponseConverterMock);
		MediaSearchRequest mediaSearchRequest = new MediaSearchRequest();
		adapter.search(mediaSearchRequest);

		Mockito.verify(mediaSearchRequestConverter).convert(mediaSearchRequest);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testMediaSearchResponseConvertion() {
		MediaProviderClient<TextConvertable, Object> mediaProviderClientMock = Mockito.mock(MediaProviderClient.class);
		Object mediaProviderClientResponse = new Object();
		Mockito.when(mediaProviderClientMock.search(Mockito.any())).thenReturn(mediaProviderClientResponse);
		MediaSearchRequestConverter<TextConvertable> mediaSearchRequestConverter = Mockito
				.mock(MediaSearchRequestConverter.class);
		Mockito.when(mediaSearchRequestConverter.convert(Mockito.any())).thenReturn(newMediaProviderSearchRequest());
		MediaProviderSearchResponseConverter<Object> mediaProviderSearchResponseConverterMock = Mockito
				.mock(MediaProviderSearchResponseConverter.class);

		MediaProviderClientAdapter<TextConvertable, Object> adapter = new MediaProviderClientAdapter<>(MediaType.BOOKS,
				mediaProviderClientMock, mediaSearchRequestConverter, mediaProviderSearchResponseConverterMock);
		adapter.search(new MediaSearchRequest());

		Mockito.verify(mediaProviderSearchResponseConverterMock).convert(mediaProviderClientResponse);
	}

	private MediaProviderSearchRequest<TextConvertable> newMediaProviderSearchRequest() {
		MediaProviderSearchRequest<TextConvertable> searchRequest = new MediaProviderSearchRequest.Builder<TextConvertable>()
				.build();
		return searchRequest;
	}
}

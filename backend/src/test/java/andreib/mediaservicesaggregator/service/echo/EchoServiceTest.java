package andreib.mediaservicesaggregator.service.echo;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import andreib.mediaservicesaggregator.Application;

@RunWith(SpringRunner.class)
@SpringBootTest
@DirtiesContext
@ActiveProfiles(Application.DEV_PROFILE)
public class EchoServiceTest {

	@Autowired
	private EchoService echoService;

	@Autowired
	private Environment environment;

	@Test
	public void testEcho() {
		String response = echoService.echo("message");
		assertThat(response, is(equalTo(environment.getProperty("echo_service_response_prefix") + "message")));
	}
}

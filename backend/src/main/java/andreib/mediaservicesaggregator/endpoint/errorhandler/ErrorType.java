package andreib.mediaservicesaggregator.endpoint.errorhandler;

public enum ErrorType {

	MEDIA_SEARCH_INVALID_REQUEST_EXCEPTION("Received request was invalid"),

	MEDIA_PROVIDER_CLIENT_TIMEOUT("Media provider didn't respond in time"),
	MEDIA_PROVIDER_CLIENT_INVALID_REQUEST("Request sent to media provider was invalid"),
	MEDIA_PROVIDER_CLIENT_ERROR("Request sent to media provider generated an error"),

	APPLICATION_ERROR("Application generated an error");

	private final String description;

	private ErrorType(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

}

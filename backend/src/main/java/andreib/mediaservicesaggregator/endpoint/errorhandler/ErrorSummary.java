package andreib.mediaservicesaggregator.endpoint.errorhandler;

public class ErrorSummary {

	private ErrorType errorType;
	private String errorTypeDescription;
	private String errorMessage;

	public ErrorSummary() {
	}

	public ErrorSummary(ErrorType errorType, String errorMessage) {
		this.errorType = errorType;
		this.errorTypeDescription = errorType.getDescription();
		this.errorMessage = errorMessage;
	}

	public ErrorType getErrorType() {
		return errorType;
	}

	public void setErrorType(ErrorType errorType) {
		this.errorType = errorType;
	}

	public String getErrorTypeDescription() {
		return errorTypeDescription;
	}

	public void setErrorTypeDescription(String errorTypeDescription) {
		this.errorTypeDescription = errorTypeDescription;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}

package andreib.mediaservicesaggregator.endpoint.errorhandler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientInvalidRequestException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientTimeoutException;
import andreib.mediaservicesaggregator.service.media.error.MediaSearchInvalidRequestException;

@ControllerAdvice
@RestController
public class ErrorHandlingEndpoint extends ResponseEntityExceptionHandler {

	@ExceptionHandler(MediaSearchInvalidRequestException.class)
	public final ResponseEntity<ErrorSummary> handleAllExceptions(MediaSearchInvalidRequestException ex,
			WebRequest request) {
		return new ResponseEntity<ErrorSummary>(
				new ErrorSummary(ErrorType.MEDIA_SEARCH_INVALID_REQUEST_EXCEPTION, ex.getCause().getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MediaProviderClientInvalidRequestException.class)
	public final ResponseEntity<ErrorSummary> handleAllExceptions(MediaProviderClientInvalidRequestException ex,
			WebRequest request) {
		return new ResponseEntity<ErrorSummary>(
				new ErrorSummary(ErrorType.MEDIA_PROVIDER_CLIENT_INVALID_REQUEST, ex.getCause().getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MediaProviderClientTimeoutException.class)
	public final ResponseEntity<ErrorSummary> handleAllExceptions(MediaProviderClientTimeoutException ex,
			WebRequest request) {
		return new ResponseEntity<ErrorSummary>(
				new ErrorSummary(ErrorType.MEDIA_PROVIDER_CLIENT_TIMEOUT, ex.getCause().getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MediaProviderClientException.class)
	public final ResponseEntity<ErrorSummary> handleAllExceptions(MediaProviderClientException ex, WebRequest request) {
		return new ResponseEntity<ErrorSummary>(
				new ErrorSummary(ErrorType.MEDIA_PROVIDER_CLIENT_ERROR, ex.getCause().getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ErrorSummary> handleAllExceptions(Exception ex, WebRequest request) {
		return new ResponseEntity<ErrorSummary>(new ErrorSummary(ErrorType.APPLICATION_ERROR, ex.getMessage()),
				HttpStatus.INTERNAL_SERVER_ERROR);
	}
}

package andreib.mediaservicesaggregator.endpoint.media;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import andreib.mediaservicesaggregator.service.media.MediaSearchRequest;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponse;
import andreib.mediaservicesaggregator.service.media.MediaService;

@RestController
@RequestMapping("/media")
public class MediaEndpoint {

	private final MediaService mediaService;

	@Autowired
	public MediaEndpoint(MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@PostMapping(path = "/search", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	public MediaSearchResponse search(@RequestBody MediaSearchRequest request) {
		return mediaService.search(request);
	}
}

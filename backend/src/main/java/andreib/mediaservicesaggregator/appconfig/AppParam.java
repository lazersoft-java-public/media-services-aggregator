package andreib.mediaservicesaggregator.appconfig;

import org.springframework.core.env.Environment;
import java.time.Duration;
import java.time.temporal.ChronoUnit;

public enum AppParam {

	REST_ENDPOINT_ALLOWED_ORIGINS("rest_endpoint_allowed_origins", ""),

	ECHO_SERVICE_RESPONSE_PREFIX("echo_service_response_prefix"),

	MEDIA_PROVIDER_CLIENT_GOOGLE_BOOKS_SEARCH_URL("media_provider_client_google_books_search_url"),
	MEDIA_PROVIDER_CLIENT_ITUNES_SEARCH_URL("media_provider_client_itunes_search_url"),

	GOOGLE_BOOKS_REST_TEMPLATE_CONNECTION_TIMEOUT("google_books_rest_template_connection_timeout", Duration.class,
			Duration.of(2, ChronoUnit.MINUTES)),
	GOOGLE_BOOKS_REST_TEMPLATE_READ_TIMEOUT("google_books_rest_template_read_timeout", Duration.class,
			Duration.of(2, ChronoUnit.MINUTES)),

	ITUNES_REST_TEMPLATE_CONNECTION_TIMEOUT("itunes_rest_template_connection_timeout", Duration.class,
			Duration.of(2, ChronoUnit.MINUTES)),
	ITUNES_REST_TEMPLATE_READ_TIMEOUT("itunes_rest_template_read_timeout", Duration.class,
			Duration.of(2, ChronoUnit.MINUTES));

	private final String name;
	private final Class<?> type;
	private final Object defaultValue;

	private AppParam(String name, Class<?> type, Object defaultValue) {
		this.name = name;
		this.type = type;
		this.defaultValue = defaultValue;
	}

	private AppParam(String name, Class<?> type) {
		this(name, type, null);
	}

	private AppParam(String name, Object defaultValue) {
		this(name, String.class, defaultValue);
	}

	private AppParam(String name) {
		this(name, String.class, null);
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return type;
	}

	public Object getDefault() {
		return defaultValue;
	}

	@SuppressWarnings("unchecked")
	public <T> T get(Environment env) {
		if (Duration.class.isAssignableFrom(type)) {
			return (T) Duration.parse(env.getProperty(name, String.class, defaultValue.toString()));
		}
		return (T) env.getProperty(name, (Class<Object>) type, defaultValue);
	}
}

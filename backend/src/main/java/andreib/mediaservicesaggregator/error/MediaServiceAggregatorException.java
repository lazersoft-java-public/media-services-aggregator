package andreib.mediaservicesaggregator.error;

@SuppressWarnings("serial")
public class MediaServiceAggregatorException extends RuntimeException {

	public MediaServiceAggregatorException(String message, Throwable cause) {
		super(message, cause);
	}

	public MediaServiceAggregatorException(String message) {
		super(message);
	}
}

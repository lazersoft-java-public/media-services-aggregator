package andreib.mediaservicesaggregator.service.echo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import andreib.mediaservicesaggregator.appconfig.AppParam;

@Service
public class EchoService {

	private String responsePrefix;

	@Autowired
	public EchoService(Environment env) {
		responsePrefix = AppParam.ECHO_SERVICE_RESPONSE_PREFIX.get(env);
	}

	public String echo(String message) {
		return responsePrefix + message;
	}
}

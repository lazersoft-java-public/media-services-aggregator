package andreib.mediaservicesaggregator.service.media.providerclientadapter.googlebooks;

import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import andreib.mediaservicesaggregator.dataaccess.googlebooks.GoogleBooksClient;
import andreib.mediaservicesaggregator.dataaccess.googlebooks.GoogleBooksSearchField;
import andreib.mediaservicesaggregator.dataaccess.googlebooks.GoogleBooksSearchResponse;
import andreib.mediaservicesaggregator.dataaccess.googlebooks.OrderBy;
import andreib.mediaservicesaggregator.dataaccess.googlebooks.Projection;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderSearchRequest;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponse;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponseEntry;
import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaProviderClientAdapter;
import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaType;

@Component
public class GoogleBooksClientAdapter
		extends MediaProviderClientAdapter<GoogleBooksSearchField, GoogleBooksSearchResponse> {

	@Autowired
	public GoogleBooksClientAdapter(GoogleBooksClient googleBooksClient) {
		super(MediaType.BOOKS, googleBooksClient, (mediaSearchRequest) -> {
			return new MediaProviderSearchRequest.Builder<GoogleBooksSearchField>()
					.setFieldValue(GoogleBooksSearchField.Q, mediaSearchRequest.getSearchTerm())
					.setFieldValue(GoogleBooksSearchField.OrderBy, OrderBy.RELEVANCE.getApiValue())
					.setFieldValue(GoogleBooksSearchField.PROJECTION, Projection.LITE.getApiValue())
					.setFieldValue(GoogleBooksSearchField.MaxResults, mediaSearchRequest.getMaxResults()).build();
		}, (googleBooksSearchResponse -> {
			return new MediaSearchResponse(googleBooksSearchResponse.getItems().stream().map(
					i -> new MediaSearchResponseEntry(i.getVolumeInfo().getTitle(), i.getVolumeInfo().getAuthors()))
					.collect(Collectors.toList()));
		}));
	}
}

package andreib.mediaservicesaggregator.service.media.providerclientadapter;

import andreib.mediaservicesaggregator.service.media.MediaSearchResponse;

@FunctionalInterface
public interface MediaProviderSearchResponseConverter<T> {

	MediaSearchResponse convert(T mediaProviderSearchResponse);
}

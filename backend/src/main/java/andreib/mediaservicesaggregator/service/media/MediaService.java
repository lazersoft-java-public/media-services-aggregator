package andreib.mediaservicesaggregator.service.media;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import andreib.mediaservicesaggregator.service.media.error.MediaSearchInvalidRequestException;
import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaProviderClientAdapterFactory;

@Service
public class MediaService {

	private final MediaProviderClientAdapterFactory mediaProviderClientAdapterFactory;

	@Autowired
	public MediaService(MediaProviderClientAdapterFactory mediaProviderClientAdapterFactory) {
		this.mediaProviderClientAdapterFactory = mediaProviderClientAdapterFactory;
	}

	public MediaSearchResponse search(MediaSearchRequest request) {
		validateRequest(request);
		return mediaProviderClientAdapterFactory.getAdapter(request.getMediaType()).search(request);
	}

	private void validateRequest(MediaSearchRequest request) {
		if (request == null) {
			throw new MediaSearchInvalidRequestException("request was empty");
		}

		if (request.getSearchTerm() == null) {
			throw new MediaSearchInvalidRequestException("requested search term was null");
		}

		if (request.getMediaType() == null) {
			throw new MediaSearchInvalidRequestException("requested media type term was null");
		}

		if (request.getMaxResults() < 0) {
			throw new MediaSearchInvalidRequestException("requested max results count was negative");
		}
	}
}

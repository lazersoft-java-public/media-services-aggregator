package andreib.mediaservicesaggregator.service.media.providerclientadapter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;

@Component
public class MediaProviderClientAdapterFactory {

	private final ApplicationContext applicationContext;

	@Autowired
	public MediaProviderClientAdapterFactory(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	@SuppressWarnings("unchecked")
	public <T extends TextConvertable, U> MediaProviderClientAdapter<T, U> getAdapter(MediaType mediaType) {
		return applicationContext.getBeansOfType(MediaProviderClientAdapter.class).values().stream()
				.filter(adapter -> adapter.getMediaType() == mediaType).findFirst().get();
	}
}

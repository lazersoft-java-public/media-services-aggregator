package andreib.mediaservicesaggregator.service.media.providerclientadapter.itunes;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import andreib.mediaservicesaggregator.dataaccess.itunes.Entity;
import andreib.mediaservicesaggregator.dataaccess.itunes.ITunesClient;
import andreib.mediaservicesaggregator.dataaccess.itunes.ITunesSearchField;
import andreib.mediaservicesaggregator.dataaccess.itunes.ITunesSearchResponse;
import andreib.mediaservicesaggregator.dataaccess.itunes.Media;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderSearchRequest;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponse;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponseEntry;
import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaProviderClientAdapter;
import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaType;

@Component
public class ITunesClientAdapter
		extends MediaProviderClientAdapter<ITunesSearchField, ITunesSearchResponse> {

	@Autowired
	public ITunesClientAdapter(ITunesClient itunesClient) {
		super(MediaType.MUSIC_ALBUMS, itunesClient, (mediaSearchRequest) -> {
			return new MediaProviderSearchRequest.Builder<ITunesSearchField>()
					.setFieldValue(ITunesSearchField.TERM, mediaSearchRequest.getSearchTerm())
					.setFieldValue(ITunesSearchField.MEDIA, Media.MUSIC.getApiValue())
					.setFieldValue(ITunesSearchField.ENTITY, Entity.ALBUM.getApiValue())
					.setFieldValue(ITunesSearchField.LIMIT, mediaSearchRequest.getMaxResults()).build();
		}, (itunesSearchResponse -> {
			return new MediaSearchResponse(itunesSearchResponse.getResults().stream().map(
					r -> new MediaSearchResponseEntry(r.getCollectionName(), Arrays.asList(r.getArtistName())))
					.collect(Collectors.toList()));
		}));
	}
}

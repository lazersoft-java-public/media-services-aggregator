package andreib.mediaservicesaggregator.service.media.providerclientadapter;

@FunctionalInterface
public interface SearchResultConverter<T, U> {

	U convert(T searchResult);
}

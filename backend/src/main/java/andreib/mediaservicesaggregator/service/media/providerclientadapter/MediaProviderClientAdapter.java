package andreib.mediaservicesaggregator.service.media.providerclientadapter;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderClient;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderSearchRequest;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;
import andreib.mediaservicesaggregator.service.media.MediaSearchRequest;
import andreib.mediaservicesaggregator.service.media.MediaSearchResponse;

public class MediaProviderClientAdapter<T extends TextConvertable, U> {
	
	private final MediaType mediaType;
	private final MediaProviderClient<T, U> mediaProviderClient;
	private final MediaSearchRequestConverter<T> mediaSearchRequestConverter;
	private final MediaProviderSearchResponseConverter<U> mediaProviderSearchResponseConverter;

	public MediaProviderClientAdapter(MediaType mediaType, MediaProviderClient<T, U> mediaProviderClient,
			MediaSearchRequestConverter<T> mediaSearchRequestConverter,
			MediaProviderSearchResponseConverter<U> mediaProviderSearchResponseConverter) {
		this.mediaType = mediaType;
		this.mediaProviderClient = mediaProviderClient;
		this.mediaSearchRequestConverter = mediaSearchRequestConverter;
		this.mediaProviderSearchResponseConverter = mediaProviderSearchResponseConverter;
	}

	public MediaSearchResponse search(MediaSearchRequest searchRequest) {
		MediaProviderSearchRequest<T> mediaProviderSearchRequest = mediaSearchRequestConverter.convert(searchRequest);
		U mediaProviderSearchResponse = mediaProviderClient.search(mediaProviderSearchRequest);
		return mediaProviderSearchResponseConverter.convert(mediaProviderSearchResponse);
	}

	public MediaType getMediaType() {
		return mediaType;
	}
}

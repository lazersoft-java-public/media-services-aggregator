package andreib.mediaservicesaggregator.service.media.error;

import andreib.mediaservicesaggregator.error.MediaServiceAggregatorException;

@SuppressWarnings("serial")
public class MediaSearchInvalidRequestException extends MediaServiceAggregatorException {

	public MediaSearchInvalidRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public MediaSearchInvalidRequestException(String message) {
		super(message);
	}

}

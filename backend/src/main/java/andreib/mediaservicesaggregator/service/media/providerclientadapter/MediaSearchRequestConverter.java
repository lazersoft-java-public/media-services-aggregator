package andreib.mediaservicesaggregator.service.media.providerclientadapter;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderSearchRequest;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;
import andreib.mediaservicesaggregator.service.media.MediaSearchRequest;

@FunctionalInterface
public interface MediaSearchRequestConverter<T extends TextConvertable> {

	MediaProviderSearchRequest<T> convert(MediaSearchRequest mediaSearchRequest);
}

package andreib.mediaservicesaggregator.service.media;

import andreib.mediaservicesaggregator.service.media.providerclientadapter.MediaType;

public class MediaSearchRequest {

	private MediaType mediaType;
	private String searchTerm;
	private int maxResults;

	public String getSearchTerm() {
		return searchTerm;
	}

	public void setSearchTerm(String searchTerm) {
		this.searchTerm = searchTerm;
	}

	public int getMaxResults() {
		return maxResults;
	}

	public void setMaxResults(int maxResults) {
		this.maxResults = maxResults;
	}

	public MediaType getMediaType() {
		return mediaType;
	}

	public void setMediaType(MediaType mediaType) {
		this.mediaType = mediaType;
	}

}

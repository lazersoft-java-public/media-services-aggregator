package andreib.mediaservicesaggregator.service.media.providerclientadapter;

public enum MediaType {
	BOOKS,
	MUSIC_ALBUMS
}

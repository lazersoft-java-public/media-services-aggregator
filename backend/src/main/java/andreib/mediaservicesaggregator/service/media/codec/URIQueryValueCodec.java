package andreib.mediaservicesaggregator.service.media.codec;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

@Service
public class URIQueryValueCodec {

	private final Map<String, String> urlReservedCharactersEncodings;

	public URIQueryValueCodec() {
		urlReservedCharactersEncodings = new HashMap<>();
		urlReservedCharactersEncodings.put("!", "%21");
		urlReservedCharactersEncodings.put("*", "%2A");
		urlReservedCharactersEncodings.put("'", "%27");
		urlReservedCharactersEncodings.put("(", "%28");
		urlReservedCharactersEncodings.put(")", "%29");
		urlReservedCharactersEncodings.put(";", "%3B");
		urlReservedCharactersEncodings.put(":", "%3A");
		urlReservedCharactersEncodings.put("@", "%40");
		urlReservedCharactersEncodings.put("&", "%26");
		urlReservedCharactersEncodings.put("=", "%3D");
		urlReservedCharactersEncodings.put("+", "%2B");
		urlReservedCharactersEncodings.put("$", "%24");
		urlReservedCharactersEncodings.put(",", "%2C");
		urlReservedCharactersEncodings.put("/", "%2F");
		urlReservedCharactersEncodings.put("?", "%3F");
		urlReservedCharactersEncodings.put("%", "%25");
		urlReservedCharactersEncodings.put("#", "%23");
		urlReservedCharactersEncodings.put("[", "%5B");
		urlReservedCharactersEncodings.put("]", "%5D");
		urlReservedCharactersEncodings.put(" ", "%20");
	}

	public String encode(String value) {
		if (value == null || value.trim().isEmpty()) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		value.codePoints().forEach(originalCodePoint -> {
			String originalCodePointAsString = new String(new int[] { originalCodePoint }, 0, 1);
			String newStringToken = urlReservedCharactersEncodings.getOrDefault(originalCodePointAsString,
					originalCodePointAsString);
			sb.append(newStringToken);
		});
		return sb.toString();
	}
}

package andreib.mediaservicesaggregator.service.media;

import java.util.List;

public class MediaSearchResponse {

	private List<MediaSearchResponseEntry> entries;
	
	public MediaSearchResponse(List<MediaSearchResponseEntry> entries) {
		this.entries = entries;
	}
	
	public MediaSearchResponse() {
		super();
	}

	public List<MediaSearchResponseEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<MediaSearchResponseEntry> entries) {
		this.entries = entries;
	}
}

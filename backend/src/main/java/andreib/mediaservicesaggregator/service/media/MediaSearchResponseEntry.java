package andreib.mediaservicesaggregator.service.media;

import java.util.List;

public class MediaSearchResponseEntry {

	private String title;
	private List<String> authorsOrArtists;

	public MediaSearchResponseEntry(String title, List<String> authorsOrArtists) {
		this.title = title;
		this.authorsOrArtists = authorsOrArtists;
	}

	public MediaSearchResponseEntry() {
		super();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<String> getAuthorsOrArtists() {
		return authorsOrArtists;
	}

	public void setAuthorsOrArtists(List<String> authorsOrArtists) {
		this.authorsOrArtists = authorsOrArtists;
	}
}

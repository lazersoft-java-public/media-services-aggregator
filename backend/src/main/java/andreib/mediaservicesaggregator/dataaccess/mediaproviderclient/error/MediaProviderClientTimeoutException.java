package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error;

@SuppressWarnings("serial")
public class MediaProviderClientTimeoutException extends MediaProviderClientException {

	public MediaProviderClientTimeoutException(String message, Throwable cause) {
		super(message, cause);
	}

}

package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error;

@SuppressWarnings("serial")
public class MediaProviderClientInvalidRequestException extends MediaProviderClientException {

	public MediaProviderClientInvalidRequestException(String message, Throwable cause) {
		super(message, cause);
	}
}

package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error;

import andreib.mediaservicesaggregator.error.MediaServiceAggregatorException;

@SuppressWarnings("serial")
public class MediaProviderClientException extends MediaServiceAggregatorException {

	public MediaProviderClientException(String message, Throwable cause) {
		super(message, cause);
	}

}

package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class MediaProviderSearchRequest<T extends TextConvertable> {

	private final Map<T, String> fieldsValuesMap;

	MediaProviderSearchRequest(Map<T, String> fieldsValuesMap) {
		Objects.nonNull(fieldsValuesMap);
		this.fieldsValuesMap = fieldsValuesMap;
	}

	public Map<T, String> getFieldsValuesMap() {
		return fieldsValuesMap;
	}

	@Override
	public int hashCode() {
		return fieldsValuesMap.hashCode();
	}

	public boolean equals(Object other) {
		if (other == null) {
			return false;
		}

		if (!(other instanceof MediaProviderSearchRequest)) {
			return false;
		}

		@SuppressWarnings("unchecked")
		MediaProviderSearchRequest<T> otherRequest = (MediaProviderSearchRequest<T>) other;
		return fieldsValuesMap.equals(otherRequest.fieldsValuesMap);
	}

	public static class Builder<T extends TextConvertable> {
		private final Map<T, String> fieldsValuesMap;

		public Builder() {
			this.fieldsValuesMap = new HashMap<>();
		}

		public Builder<T> setFieldValue(T field, String value) {
			Objects.requireNonNull(value);
			fieldsValuesMap.put(field, value);
			return this;
		}

		public Builder<T> setFieldValue(T field, Integer value) {
			Objects.requireNonNull(value);
			fieldsValuesMap.put(field, Integer.toString(value, 10));
			return this;
		}

		public MediaProviderSearchRequest<T> build() {
			return new MediaProviderSearchRequest<>(Collections.unmodifiableMap(fieldsValuesMap));
		}
	}
}

package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient;

import java.nio.charset.Charset;

import org.springframework.http.MediaType;
import org.springframework.http.converter.json.AbstractJackson2HttpMessageConverter;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

public class TextJavascriptConverter extends AbstractJackson2HttpMessageConverter {
	
	public TextJavascriptConverter() {
		super(new Jackson2ObjectMapperBuilder().failOnUnknownProperties(false).build(), new MediaType("text", "javascript", Charset.forName("UTF-8")));
	}
}

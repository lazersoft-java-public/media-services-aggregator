package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient;

import java.net.SocketTimeoutException;
import java.net.URI;
import java.util.stream.Collectors;

import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientInvalidRequestException;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.error.MediaProviderClientTimeoutException;
import andreib.mediaservicesaggregator.service.media.codec.URIQueryValueCodec;

public abstract class MediaProviderClient<T extends TextConvertable, U> {

	private final RestTemplate restTemplate;
	private final HttpMethod httpMethod;
	private final String baseUrl;
	private final Class<U> responseClass;

	protected MediaProviderClient(RestTemplate restTemplate, HttpMethod httpMethod, String baseUrl,
			Class<U> responseClass) {
		this.restTemplate = restTemplate;
		this.httpMethod = httpMethod;
		this.baseUrl = baseUrl;
		this.responseClass = responseClass;
	}

	public U search(MediaProviderSearchRequest<T> request) {
		String url = addQueryParams(baseUrl, request);
		ResponseEntity<U> responseEntity;
		try {
			responseEntity = restTemplate.exchange(URI.create(url), httpMethod, null, responseClass);
		} catch (RestClientException e) {
			MediaProviderClientException mediaProviderClientException = handleException(e);
			throw mediaProviderClientException;
		}
		return responseEntity.getBody();
	}

	protected <V extends MediaProviderClientException> V handleException(RestClientException restClientException) {
		if (SocketTimeoutException.class.isInstance(restClientException.getCause())) {
			throw new MediaProviderClientTimeoutException(restClientException.getMessage(), restClientException);
		} else if (HttpClientErrorException.BadRequest.class.isInstance(restClientException)) {
			throw new MediaProviderClientInvalidRequestException(restClientException.getMessage(), restClientException);
		} else {
			throw new MediaProviderClientException(restClientException.getMessage(), restClientException);
		}
	}

	protected String addQueryParams(String url, MediaProviderSearchRequest<T> request) {
		URIQueryValueCodec codec = new URIQueryValueCodec();
		StringBuilder sb = new StringBuilder(url);
		if (!request.getFieldsValuesMap().isEmpty()) {
			sb.append("?");
			sb.append(request.getFieldsValuesMap().entrySet().stream()
					.map(e -> e.getKey().getText() + "=" + codec.encode(e.getValue()))
					.collect(Collectors.joining("&")));
		}
		return sb.toString();
	}
}

package andreib.mediaservicesaggregator.dataaccess.mediaproviderclient;

public interface TextConvertable {

	String getText();
	
}

package andreib.mediaservicesaggregator.dataaccess.itunes;

public enum Media {
	MUSIC("music");

	private final String apiValue;

	private Media(String apiValue) {
		this.apiValue = apiValue;
	}

	public String getApiValue() {
		return apiValue;
	}
}

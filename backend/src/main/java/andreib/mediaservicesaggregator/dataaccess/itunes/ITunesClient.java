package andreib.mediaservicesaggregator.dataaccess.itunes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import andreib.mediaservicesaggregator.appconfig.AppParam;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderClient;

@Repository
public class ITunesClient extends MediaProviderClient<ITunesSearchField, ITunesSearchResponse> {

	@Autowired
	public ITunesClient(@Qualifier("ITunesRestTemplate") RestTemplate restTemplate, Environment env) {
		super(restTemplate, HttpMethod.GET, AppParam.MEDIA_PROVIDER_CLIENT_ITUNES_SEARCH_URL.get(env), ITunesSearchResponse.class);
	}
}

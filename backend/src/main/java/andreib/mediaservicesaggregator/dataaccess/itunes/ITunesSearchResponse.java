package andreib.mediaservicesaggregator.dataaccess.itunes;

import java.util.ArrayList;
import java.util.List;

public class ITunesSearchResponse {
	private long resultCount;
	private List<Result> results;

	public long getResultCount() {
		return resultCount;
	}

	public void setResultCount(long resultCount) {
		this.resultCount = resultCount;
	}

	public List<Result> getResults() {
		return results == null ? new ArrayList<>() : results;
	}

	public void setResults(List<Result> results) {
		this.results = results;
	}

}

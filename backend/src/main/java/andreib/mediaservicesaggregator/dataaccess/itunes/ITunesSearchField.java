package andreib.mediaservicesaggregator.dataaccess.itunes;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;

public enum ITunesSearchField implements TextConvertable {
	TERM("term"), MEDIA("media"), LIMIT("limit"), ENTITY("entity");

	private final String name;

	private ITunesSearchField(String name) {
		this.name = name;
	}

	@Override
	public String getText() {
		return name;
	}
}

package andreib.mediaservicesaggregator.dataaccess.itunes;

public enum Entity {
	ALBUM("album");

	private final String apiValue;

	private Entity(String apiValue) {
		this.apiValue = apiValue;
	}

	public String getApiValue() {
		return apiValue;
	}
}

package andreib.mediaservicesaggregator.dataaccess.googlebooks;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import andreib.mediaservicesaggregator.appconfig.AppParam;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.MediaProviderClient;

@Repository
public class GoogleBooksClient extends MediaProviderClient<GoogleBooksSearchField, GoogleBooksSearchResponse> {

	@Autowired
	public GoogleBooksClient(@Qualifier("GoogleBooksRestTemplate") RestTemplate restTemplate, Environment env) {
		super(restTemplate, HttpMethod.GET, AppParam.MEDIA_PROVIDER_CLIENT_GOOGLE_BOOKS_SEARCH_URL.get(env), GoogleBooksSearchResponse.class);
	}
}

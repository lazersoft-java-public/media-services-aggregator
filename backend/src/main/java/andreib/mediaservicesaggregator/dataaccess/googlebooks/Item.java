package andreib.mediaservicesaggregator.dataaccess.googlebooks;

public class Item {
	private String kind;
	private VolumeInfo volumeInfo;

	public String getKind() {
		return kind;
	}

	public void setKind(String kind) {
		this.kind = kind;
	}

	public VolumeInfo getVolumeInfo() {
		return volumeInfo;
	}

	public void setVolumeInfo(VolumeInfo volumeInfo) {
		this.volumeInfo = volumeInfo;
	}
}

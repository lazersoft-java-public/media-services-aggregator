package andreib.mediaservicesaggregator.dataaccess.googlebooks;

public enum OrderBy {

	RELEVANCE("relevance"), NEWEST("newest");

	private final String apiValue;

	private OrderBy(String apiValue) {
		this.apiValue = apiValue;
	}

	public String getApiValue() {
		return apiValue;
	}
}

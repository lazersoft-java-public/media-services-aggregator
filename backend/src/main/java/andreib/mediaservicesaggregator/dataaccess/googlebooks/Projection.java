package andreib.mediaservicesaggregator.dataaccess.googlebooks;

public enum Projection {
	FULL("full"), LITE("lite");

	private final String apiValue;

	private Projection(String apiValue) {
		this.apiValue = apiValue;
	}

	public String getApiValue() {
		return apiValue;
	}
}

package andreib.mediaservicesaggregator.dataaccess.googlebooks;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class GoogleBooksSearchResponse {
	private long totalHits;
	private List<Item> items;

	public long getTotalHits() {
		return totalHits;
	}

	public void setTotalHits(long totalHits) {
		this.totalHits = totalHits;
	}

	public List<Item> getItems() {
		return items == null ? new ArrayList<>() : items;
	}

	public void setItems(List<Item> items) {
		this.items = items;
	}

}

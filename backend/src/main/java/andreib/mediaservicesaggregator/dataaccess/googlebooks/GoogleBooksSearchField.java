package andreib.mediaservicesaggregator.dataaccess.googlebooks;

import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextConvertable;

public enum GoogleBooksSearchField implements TextConvertable {
	Q("q"), OrderBy("orderBy"), MaxResults("maxResults"), PROJECTION("projection");

	private final String name;

	private GoogleBooksSearchField(String name) {
		this.name = name;
	}

	@Override
	public String getText() {
		return name;
	}
}

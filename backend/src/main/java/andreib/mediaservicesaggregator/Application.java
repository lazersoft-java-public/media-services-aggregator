package andreib.mediaservicesaggregator;

import java.nio.charset.Charset;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import andreib.mediaservicesaggregator.appconfig.AppParam;
import andreib.mediaservicesaggregator.dataaccess.mediaproviderclient.TextJavascriptConverter;

@SpringBootApplication
public class Application {

	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

	public static final String DEV_PROFILE = "dev";
	public static final String PROD_PROFILE = "prod";

	@Autowired
	private Environment env;

	@Bean("GoogleBooksRestTemplate")
	public RestTemplate getGoogleBooksRestTemplate(RestTemplateBuilder restTemplateBuilder) {
		return restTemplateBuilder.setConnectTimeout(AppParam.GOOGLE_BOOKS_REST_TEMPLATE_CONNECTION_TIMEOUT.get(env))
				.setReadTimeout(AppParam.GOOGLE_BOOKS_REST_TEMPLATE_READ_TIMEOUT.get(env)).build();
	}

	@Bean("ITunesRestTemplate")
	public RestTemplate getITunesRestTemplate(RestTemplateBuilder restTemplateBuilder) {
		RestTemplate restTemplate = restTemplateBuilder
				.setConnectTimeout(AppParam.ITUNES_REST_TEMPLATE_CONNECTION_TIMEOUT.get(env))
				.setReadTimeout(AppParam.ITUNES_REST_TEMPLATE_READ_TIMEOUT.get(env)).build();
		restTemplate.getMessageConverters().add(new TextJavascriptConverter());
		return restTemplate;
	}

	@Bean
	@Profile(DEV_PROFILE)
	public WebMvcConfigurer corsConfigurer() {
		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedMethods(HttpMethod.HEAD.name(), HttpMethod.OPTIONS.name(), HttpMethod.GET.name(),
								HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod.DELETE.name())
						.allowedOrigins((String) AppParam.REST_ENDPOINT_ALLOWED_ORIGINS.get(env));
			}
		};
	}

	public static void main(String[] args) {
		LOGGER.info("Default encoding is: " + Charset.defaultCharset());
		if (!Charset.defaultCharset().equals(Charset.forName("UTF-8"))) {
			LOGGER.error("JVM default Charset encoding needs to be set to UTF-8, before starting the application. "
					+ "Use command line argument: -Dfile.encoding=\"UTF-8\" when launching the application");
			System.exit(1);
		}
		SpringApplication.run(Application.class, args);
	}

}